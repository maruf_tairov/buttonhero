<?php namespace App\Repositories;

use App\Patient;

class PatientsRepository {

    public $patient;

    public function __construct (Patient $patient) {
        $this->patient = $patient;
    }

    public function patients () {
        return $this->patient->get();
    }

    public function patient ($key) {
        return $this->patient->whereKey($key)->first();
    }

    public function insert ($data) {
        return $this->patient->insert($data);
    }

    public function update ($key, $data) {
        return $this->patient->whereKey($key)->update($data);
    }

    public function destroy ($key) {
        return $this->patient->whereKey($key)->delete();
    }
}