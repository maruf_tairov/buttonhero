<?php namespace App\Repositories;

use App\Danger;

class EventsRepository {

    public  $event;

    public function __construct (Danger $danger) {
        $this->event = $danger;
    }

    public function shown ($id) {
        return $this->event->where('id', $id)->update(['status' => 1]);
    }
}