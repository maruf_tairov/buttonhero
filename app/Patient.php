<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    public function danger () {
        return $this->hasMany('App\Danger');
    }
}
