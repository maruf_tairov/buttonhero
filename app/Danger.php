<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Danger extends Model {
    protected $table = 'events';
    protected $fillable = ['patient_key', 'long', 'lat', 'status'];

    public function patient () {
        return $this->belongsTo('App\Patient');
    }
}