<?php

Route::get('/', function () { return redirect(route('login'));});

Route::get('/login', ['as'=>'login', 'uses'=>'Auth\LoginController@index']);
Route::post('/login/auth', ['as'=>'login.auth', 'uses'=>'Auth\LoginController@auth']);
Route::get('/logout', ['as'=>'logout', 'uses'=>'Auth\LoginController@logout']);

Route::get('/map', ['as'=>'clinic', 'uses'=>'HomeController@index']);

Route::group(['prefix' => 'patients'], function () {
    Route::get('/all', ['as'=>'patients', 'uses'=>'PatientsController@index']);
    Route::get('/show/{key}', ['as'=>'patients.show', 'uses'=>'PatientsController@show']);
    Route::get('/create', ['as'=>'patients.create', 'uses'=>'PatientsController@create']);
    Route::post('/store', ['as'=>'patients.store', 'uses'=>'PatientsController@store']);
    Route::get('/edit/{key}', ['as'=>'patients.edit', 'uses'=>'PatientsController@edit']);
    Route::post('/update/{key}', ['as'=>'patients.update', 'uses'=>'PatientsController@update']);
    Route::get('/delete/{key}', ['as'=>'patients.delete', 'uses'=>'PatientsController@destroy']);
});

Route::group(['prefix' => 'api'], function () {
    Route::any('event', ['name' => 'api.event', 'uses' => 'ApiController@event']);
    Route::any('get', ['name' => 'api.get', 'uses' => 'ApiController@get']);
    Route::any('key/{key}', ['name' => 'api.key', 'uses' => 'ApiController@key']);
    Route::any('event/complete', ['name' => 'api.event.complete', 'uses' => 'ApiController@complete']);
});

Route::get('/clear', 'PatientsController@clear');