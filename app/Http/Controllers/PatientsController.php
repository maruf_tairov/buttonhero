<?php

namespace App\Http\Controllers;

use App\Repositories\PatientsRepository;

use App\Danger;
use Illuminate\Database\QueryException;
use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PatientsController extends Controller {

    public $patients;

    public function __construct (PatientsRepository $patientsRepository) {
        $this->patients = $patientsRepository;
    }

    public function index () {
        $patients = $this->patients->patients();

        return view('patients.grid', compact('patients'));
    }

    public function create () {
        return view('patients.create');
    }

    public function store () {
        $request = Request::only('full_name', 'adress', 'birth_date', 'phone', 'blood_group');

        $request['key'] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 6);;

        try {
            $this->patients->insert($request);
            return redirect(route('patients'));
        } catch (QueryException $e) {
            return $e;
        }
    }

    public function show ($key) {
        $patient = $this->patients->patient($key);

        return view('patients.show', compact('patient'));
    }

    public function edit ($key) {
        $patient = $this->patients->patient($key);

        return view('patients.edit', compact('patient'));
    }

    public function update ($key) {
        $request = Request::only('full_name', 'adress', 'birth_date', 'phone', 'blood_group');

        try {
            $this->patients->update($key, $request);
            return redirect(route('patients'));
        } catch (QueryException $e) {
            return $e;
        }
    }

    public function destroy ($key) {
        try {
            $this->patients->destroy($key);
            return redirect(route('patients'));
        } catch (QueryException $e) {
            return $e;
        }
    }

    public function clear () {
        try {
            Danger::truncate();

            return redirect('/');
        } catch (\Exception $e) {
            return $e;
        }
    }
}
