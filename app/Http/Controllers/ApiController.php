<?php

namespace App\Http\Controllers;

use App\Repositories\EventsRepository;
use App\Repositories\PatientsRepository;
use App\Device;
use App\Danger;

class ApiController extends Controller {

    public $patient;
    public $event;

    public function __construct (
        PatientsRepository $patientsRepository,
        EventsRepository $eventsRepository
    ) {
        $this->patient = $patientsRepository;
        $this->event = $eventsRepository;
    }
    
    public function event () {

        $key = \Input::get('key');

        $patient = $this->patient->patient($key);

        $request = [
            'patient_id' => $patient->id,
            'long' => \Input::get('long'),
            'lat' => \Input::get('lat'),
            'status' => 0

        ];



        try {
            Danger::insert($request);

            return [
                'success' => true
            ];
        } catch (\Exception $e) {
            var_dump($e);
            return [
                'success' => false
            ];
        }
    }

    public function complete () {
        $id = \Input::get('id');

        $this->event->shown($id);
        return 'changed';
    }

    public function get () {
        $ids = \Input::get('ids');

        $event = Danger::where('status', 0)->whereNotIn('id', $ids)->with('patient')->get();

        return $event;

    }

    public function key ($key) {
        $data = $this->patient->patient($key);

        if ($data) {
            return $data;
        } else {
            return 'User is not available';
        }
    }
}