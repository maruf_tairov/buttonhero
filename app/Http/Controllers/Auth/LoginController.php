<?php namespace App\Http\Controllers\Auth;

use Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class LoginController extends Controller {

    public function index () {
        if (Auth::check()) {
            return redirect(route('clinic'));
        }

        return view('auth.login');
    }

    public function auth (){
        $request = Request::only('username', 'password');

        if (Auth::attempt($request)) {
            return redirect(route('clinic'));
        } else {
            return redirect()->back()->withInput()->with('message', 'Login Failed');;
        }
    }

    public function logout () {
        Auth::logout();

        return redirect(route('login'));
    }
}
