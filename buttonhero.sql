-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 19 2016 г., 05:46
-- Версия сервера: 5.6.17
-- Версия PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `buttonhero`
--

-- --------------------------------------------------------

--
-- Структура таблицы `clinics`
--

CREATE TABLE IF NOT EXISTS `clinics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `clinics`
--

INSERT INTO `clinics` (`id`, `name`, `username`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Городская клиническая больница скорой медицинской помощи', 'emergency', '$2y$10$jY2SKB/DBlwIO0u/ZrBXs.FPEe2aKyT7XP2pzpUue8R7/38ls9nyO', 1, '', '2016-05-18 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_key` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `long` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `events`
--

INSERT INTO `events` (`id`, `patient_key`, `lat`, `long`, `status`, `created_at`, `updated_at`) VALUES
(7, '123456', '64.420101', '39.772622', 0, '2016-05-18 18:03:20', '2016-05-18 18:03:20');

-- --------------------------------------------------------

--
-- Структура таблицы `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `blood_group` int(1) NOT NULL,
  `phone` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `patients`
--

INSERT INTO `patients` (`id`, `full_name`, `adress`, `birth_date`, `blood_group`, `phone`, `key`, `status`) VALUES
(1, 'Nazarbekov Ozodbek', '', '0000-00-00', 3, 0, 'vdvd415dv5d2', 1),
(2, '1', '2', '2016-12-31', 1, 23121231, 'fpqsad', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
