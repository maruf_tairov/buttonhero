@extends('layout')

@section('css')
    <link rel="stylesheet" href="/css/leaflet.css"/>
@endsection

@section('content')
    <div id="map"></div>
@endsection

@section('js')
    <script type="text/javascript" src="/js/leaflet.js"></script>

    <script type="text/javascript">
        var map = L.map('map').setView([39.7638772,64.4216289], 14);

        var markers = [ ];

        var ids = [0];

        $(document).on("click", '.accepted', function () {

            var id = $(this).data("id");
            var marker = markers[$(this).data("marker")];

            $.ajax({
                url: "/api/event/complete",
                type: "GET",
                data: {'id': id},
                dataType: "json",
                success: function () {
                    map.removeLayer(marker);
                }
            });
        });

        function addId(value) {
            ids.push(value);
        }

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map);

        setInterval(function () {
            $.ajax({
                url: "/api/get",
                type: "GET",
                data: { 'ids' : ids},
                dataType: "json",
                success: function (json) {
                    for (item in json) {
                        var marker = L.marker([json[item].long, json[item].lat]).addTo(map);
                        var key = markers.push(marker) - 1;
                        marker.bindPopup(
                            '<b>' + json[item].patient.full_name + '</b> <br><br>' +
                            '<a data-marker="' + key + '" data-id="'+json[item].id+'" class="btn btn-success btn-flat accepted"><i class="glyphicon glyphicon-forward"></i> Принял</a>'
                        ).openPopup();

                        addId(json[item].id);
                    }
                }
            });
        }, 5000);

        function remove (id) {
            $.ajax({
                url: "/api/event/complete",
                type: "GET",
                data: { 'id' : 'id' },
                dataType: "json",
                success: console.log('1')
            });
        }
    </script>
@endsection;