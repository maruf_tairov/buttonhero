<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ButtonHero</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/style.css"/>
    @yield('css')
</head>
<body>
<div>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-7" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a target="_blank" class="navbar-brand" href="#">ButtonHero</a>

            </div>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('clinic') }}"><i class="glyphicon glyphicon-map-marker"></i> Карта</a></li>
                <li><a href="{{ route('patients') }}"><i class="glyphicon glyphicon-user"></i> Пациенты</a></li>
                <li><a href="{{ route('logout') }}"><i class="glyphicon glyphicon-off"></i> Выход</a></li>
            </ul>
        </div>
    </nav>
</div>

@yield('content')

<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/bootstrap.js"></script>

@yield('js')

</body>
</html>