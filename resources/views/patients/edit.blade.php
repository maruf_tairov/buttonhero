@extends('../layout')

@section('content')
    <div class="page">
        <div class="container">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel panel-heading">
                        <i class="glyphicon glyphicon-user"></i>
                        {{ $patient->full_name }}
                    </div>

                    <div class="panel panel-body">
                        <form action="{{ route('patients.update', ['key' => $patient->key]) }}" method="POST">

                            <div class="pull-right">
                                <a class="btn btn-warning btn-flat" href="{{ route('patients') }}"><i class="glyphicon glyphicon-remove"></i> Отменить</a>
                                <button class="btn btn-primary btn-flat"><i class="glyphicon glyphicon-floppy-disk"></i> Сохранить</button>
                            </div>

                            @include('patients._form', ['action' => 'edit'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection