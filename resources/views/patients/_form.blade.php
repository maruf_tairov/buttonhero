<div class="col-md-8">
    <div class="form-group">
        <label for="full_name">ФИО</label>
        <input type="text" name="full_name" class="form-control" value="@if($action == 'edit'){{ $patient->full_name }}@endif">
    </div>

    <div class="form-group">
        <label for="adress">Адрес</label>
        <input type="text" name="adress" class="form-control" value="@if($action == 'edit'){{ $patient->adress }}@endif">
    </div>

    <div class="form-group">
        <label for="birth_date">Дата рождения</label>
        <input type="date" name="birth_date" class="form-control" value="@if($action == 'edit'){{ $patient->birth_date }}@endif">
    </div>

    <div class="form-group">
        <label for="phone">Телефон</label>
        <input type="number" placeholder="998 91 453 99 98" name="phone" class="form-control" value="@if($action == 'edit'){{ $patient->phone }}@endif">
    </div>

    <div class="form-group">
        <label for="blood_group">Группа кровьи</label>
        <select class="form-control" name="blood_group">
            <option @if($action == 'edit' && $patient->blood_group == 1){{ 'selected' }}@endif value="1">I</option>
            <option @if($action == 'edit' && $patient->blood_group == 2){{ 'selected' }}@endif selected value="2">II</option>
            <option @if($action == 'edit' && $patient->blood_group == 3){{ 'selected' }}@endif value="3">III</option>
            <option @if($action == 'edit' && $patient->blood_group == 4){{ 'selected' }}@endif value="4">IV</option>
        </select>
    </div>

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

</div>
