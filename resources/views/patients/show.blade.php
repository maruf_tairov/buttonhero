@extends('.layout')

@section('content')
    <div class="page">
        <div class="container">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel panel-heading">
                        <i class="glyphicon glyphicon-user"></i>
                        {{ $patient->full_name }}
                    </div>

                    <div class="panel panel-body">
                        <div class="pull-right">
                            <a href="{{ route('patients') }}" class="btn btn-warning btn-flat" href=""><i class="glyphicon glyphicon-backward"></i> Назад</a>
                            <a href="{{ route('patients.edit', ['key' => $patient->key]) }}" class="btn btn-info btn-flat" href=""><i class="glyphicon glyphicon-pencil"></i> Редактировать</a>
                            <a href="{{ route('patients.delete', ['key' => $patient->key]) }}" class="btn btn-danger btn-flat" href=""><i class="glyphicon glyphicon-trash"></i> Удалить</a>
                        </div>

                        <?php $blood = ['-', 'I', 'II', 'III', 'IV']; ?>

                        <div class="col-md-8">
                            <div>
                                <b class="col-md-6">ФИО:</b> {{ $patient->full_name }}
                            </div>

                            <div>
                                <b class="col-md-6">Дата рождения:</b> {{ $patient->birth_date }}
                            </div>

                            <div>
                                <b class="col-md-6">Адрес:</b> {{ $patient->adress }}
                            </div>

                            <div>
                                <b class="col-md-6">Телефон:</b> {{ $patient->phone }}
                            </div>

                            <div>
                                <b class="col-md-6">Группа кровьи:</b> {{ $blood[$patient->blood_group] }}
                            </div>

                            <div>
                                <b class="col-md-6">Ключь:</b> {{ $patient->key }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection