@extends('../layout')

@section('content')
    <div class="page">
        <div class="container">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel panel-heading">
                        <i class="glyphicon glyphicon-user"></i>
                        Пациенты
                    </div>

                    <div class="panel panel-body">
                        <div class="pull-right">
                            <a href="{{ route('patients.create') }}" class="btn btn-primary btn-flat" href=""><i class="glyphicon glyphicon-plus"></i> Добавить</a>
                        </div>


                        <div>
                            <table class="table table-hover table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ФИО</th>
                                    <th>Дата рождения</th>
                                    <th>Адрес</th>
                                    <th>Телефон</th>
                                    <th>Группа кровьи</th>
                                    <th>Ключь</th>
                                    <th>Действии</th>
                                </tr>
                                </thead>

                                <?php
                                    $i = 1;
                                    $blood = ['-', 'I', 'II', 'III', 'IV'];
                                ?>

                                <tbody>
                                @foreach($patients as $patient)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $patient->full_name }}</td>
                                        <td>{{ $patient->birth_date }}</td>
                                        <td>{{ $patient->adress }}</td>
                                        <td>{{ $patient->phone }}</td>
                                        <td>{{ $blood[$patient->blood_group] }}</td>
                                        <td>{{ $patient->key }}</td>
                                        <td>
                                            <a class="btn btn-success btn-flat btn-sm" href="{{ route('patients.show', ['key' => $patient->key]) }}"><i class="glyphicon glyphicon-search"></i></a>
                                            <a class="btn btn-info btn-flat btn-sm" href="{{ route('patients.edit', ['key' => $patient->key]) }}"><i class="glyphicon glyphicon-pencil"></i></a>
                                            <a class="btn btn-danger btn-flat btn-sm" href="{{ route('patients.delete', ['key' => $patient->key]) }}"><i class="glyphicon glyphicon-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection