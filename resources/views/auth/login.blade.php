<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ButtonHero</title>

    <link rel="stylesheet" href="/css/login.css">
</head>

<body>
    <div class="login-page">
        <div class="form">
            <form action="{{ route('login.auth') }}" method="post" class="login-form">
                <input name="username" type="text" placeholder="Ваш логин"/>
                <input name="password" type="password" placeholder="Ваш пароль"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button>Вход</button>
            </form>
        </div>
    </div>
</body>
</html>